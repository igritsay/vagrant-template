<?php

$cfg['Lang'] = 'en-utf-8';

$cfg['Export']['method'] = 'custom';
$cfg['Export']['sql_include_comments'] = false;
$cfg['Export']['sql_drop_table'] = true;
$cfg['Export']['sql_disable_fk'] = true;