#!/usr/bin/env bash

# By storing the date now, we can calculate the duration of provisioning at the
# end of this script.
start_seconds="$(date +%s)"

# Open HTTP and HTTPS ports
sudo ufw allow 80
sudo ufw allow 443

# Network Detection
#
# Make an HTTP request to google.com to determine if outside access is available
# to us. If 3 attempts with a timeout of 5 seconds are not successful, then we'll
# skip a few things further in provisioning rather than create a bunch of errors.
if [[ "$(wget --tries=3 --timeout=20 --spider http://google.com 2>&1 | grep 'connected')" ]]; then
	echo "Network connection detected..."
	ping_result="Connected"
else
	echo "Network connection not detected. Unable to reach google.com..."
	ping_result="Not Connected"
fi

# PACKAGE INSTALLATION
#
# Build a bash array to pass all of the packages we want to install to a single
# apt-get command. This avoids doing all the leg work each time a package is
# set to install. It also allows us to easily comment out or add single
# packages. We set the array as empty to begin with so that we can append
# individual packages to it as required.
apt_package_install_list=()

# Start with a bash array containing all packages we want to install in the
# virtual machine. We'll then loop through each of these and check individual
# status before adding them to the apt_package_install_list array.
apt_package_check_list=(
	apache2
	php5
	php5-common
	libapache2-mod-php5
	libapache2-mod-auth-mysql
	php5-cli
	php5-dev
	php5-memcache
	php5-imagick
	php5-mcrypt
	php5-mysql
	php5-imap
	php5-curl
	php-pear
	php5-gd
	php5-ldap
	memcached
	mysql-server
	imagemagick
	subversion
	git-core
	zip
	unzip
	ngrep
	curl
	make
	vim
	colordiff
	gettext
	dos2unix
	g++
	nodejs
	phpmyadmin
)

echo "Check for apt packages to install..."

# Loop through each of our packages that should be installed on the system. If
# not yet installed, it should be added to the array of packages to install.
for pkg in "${apt_package_check_list[@]}"; do
	package_version="$(dpkg -s $pkg 2>&1 | grep 'Version:' | cut -d " " -f 2)"
	if [[ -n "${package_version}" ]]; then
		space_count="$(expr 20 - "${#pkg}")" #11
		pack_space_count="$(expr 30 - "${#package_version}")"
		real_space="$(expr ${space_count} + ${pack_space_count} + ${#package_version})"
		printf " * $pkg %${real_space}.${#package_version}s ${package_version}\n"
	else
		echo " *" $pkg [not installed]
		apt_package_install_list+=($pkg)
	fi
done

# MySQL
#
# Use debconf-set-selections to specify the default password for the root MySQL
# account. This runs on every provision, even if MySQL has been installed. If
# MySQL is already installed, it will not affect anything.
echo mysql-server mysql-server/root_password password root | debconf-set-selections
echo mysql-server mysql-server/root_password_again password root | debconf-set-selections

# Use debconf-set-selections to specify settings for the phpMyAdmin
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" | debconf-set-selections
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-user string root" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password root" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password phpma" |debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password phpma" | debconf-set-selections

# Provide our custom apt sources before running `apt-get update`
ln -sf /srv/app/apt-source-append.list /etc/apt/sources.list.d/vagrant-sources.list
echo "Linked custom apt sources"


if [[ $ping_result == "Connected" ]]; then
	# If there are any packages to be installed in the apt_package_list array,
	# then we'll run `apt-get update` and then `apt-get install` to proceed.
	if [[ ${#apt_package_install_list[@]} = 0 ]]; then
		echo -e "No apt packages to install.\n"
	else
		# Before running `apt-get update`, we should add the public keys for
		# the packages that we are installing from non standard sources via
		# our appended apt source.list

		# Apply the nodejs assigning key
		echo "Applying nodejs signing key..."
		apt-key adv --quiet --keyserver hkp://keyserver.ubuntu.com:80 --recv-key C7917B12 2>&1 | grep "gpg:"
		apt-key export C7917B12 | apt-key add -

		# update all of the package references before installing anything
		echo "Running apt-get update..."
		apt-get update --assume-yes

		# install required packages
		echo "Installing apt-get packages..."
		apt-get install --assume-yes ${apt_package_install_list[@]}

		# Clean up apt caches
		apt-get clean
	fi

	# Make sure we have the latest npm version
	npm install -g npm

	# xdebug
	#
	# XDebug 2.2.3 is provided with the Ubuntu install by default. The PECL
	# installation allows us to use a later version. Not specifying a version
	# will load the latest stable.
	pecl install xdebug

	# COMPOSER
	#
	# Install Composer if it is not yet available.
	if [[ ! -n "$(composer --version --no-ansi | grep 'Composer version')" ]]; then
		echo "Installing Composer..."
		curl -sS https://getcomposer.org/installer | php
		chmod +x composer.phar
		mv composer.phar /usr/local/bin/composer
	fi

	# Update both Composer and any global packages. Updates to Composer are direct from
	# the master branch on its GitHub repository.
	if [[ -n "$(composer --version --no-ansi | grep 'Composer version')" ]]; then
		echo "Updating Composer..."
		COMPOSER_HOME=/usr/local/src/composer composer self-update
		COMPOSER_HOME=/usr/local/src/composer composer -q global require --no-update phpunit/phpunit:4.3.*
		COMPOSER_HOME=/usr/local/src/composer composer -q global require --no-update phpunit/php-invoker:1.1.*
		COMPOSER_HOME=/usr/local/src/composer composer -q global require --no-update mockery/mockery:0.9.*
		COMPOSER_HOME=/usr/local/src/composer composer -q global require --no-update d11wtq/boris:v1.0.8
		COMPOSER_HOME=/usr/local/src/composer composer -q global config bin-dir /usr/local/bin
		COMPOSER_HOME=/usr/local/src/composer composer global update
	fi

	# Grunt
	#
	# Install or Update Grunt based on current state.  Updates are direct
	# from NPM
	if [[ "$(grunt --version)" ]]; then
		echo "Updating Grunt CLI"
		npm update -g grunt-cli &>/dev/null
		npm update -g grunt-sass &>/dev/null
		npm update -g grunt-cssjanus &>/dev/null
	else
		echo "Installing Grunt CLI"
		npm install -g grunt-cli &>/dev/null
		npm install -g grunt-sass &>/dev/null
		npm install -g grunt-cssjanus &>/dev/null
	fi

	#phpMyAdmin
	sed -i -r "s:(Alias /).*(/usr/share/phpmyadmin):\1phpma \2:" /etc/phpmyadmin/apache.conf
	sudo cp /vagrant/vagrant/phpma.config.inc.php /etc/phpmyadmin/conf.d/vagrant.inc.php

	#Drush
	pear channel-discover pear.drush.org
	pear install drush/drush

else
	echo -e "\nNo network connection available, skipping package installation"
fi

echo -e "\nSetup configuration files..."

# Used to to ensure proper services are started on `vagrant up`
cp /vagrant/vagrant/vagrant-start.conf /etc/init/vagrant-start.conf

echo " * /srv/app/vagrant-start.conf               -> /etc/init/vagrant-start.conf"

# Enable apache modules
echo -e "\nEnable apache modules..."

a2enmod rewrite
a2enmod vhost_alias

echo -e "\nSetup apache vhosts file links..."
cp /vagrant/vagrant/vhost-lvh.me.conf /etc/apache2/sites-available/vhost-lvh.me.conf
cp /vagrant/vagrant/phpma.lvh.me.conf /etc/apache2/sites-available/phpma.lvh.me.conf

echo -e "\nEnable apache vhosts..."
a2ensite vhost-lvh.me.conf
a2ensite phpma.lvh.me.conf


# RESTART SERVICES
#
# Make sure the services we expect to be running are running.
echo -e "\nRestart services..."
service apache2 restart

# If MySQL is installed, go through the various imports and service tasks.
exists_mysql="$(service mysql status)"
if [[ "mysql: unrecognized service" != "${exists_mysql}" ]]; then
	echo -e "\nSetup MySQL configuration file links..."

	# Copy mysql configuration from local
	cp /vagrant/vagrant/my.cnf /etc/mysql/my.cnf
	cp /vagrant/vagrant/root-my.cnf /home/vagrant/.my.cnf

	echo " * /srv/app/my.cnf               -> /etc/mysql/my.cnf"
	echo " * /srv/app/root-my.cnf          -> /home/vagrant/.my.cnf"

	# MySQL gives us an error if we restart a non running service, which
	# happens after a `vagrant halt`. Check to see if it's running before
	# deciding whether to start or restart.
	if [[ "mysql stop/waiting" == "${exists_mysql}" ]]; then
		echo "service mysql start"
		service mysql start
	else
		echo "service mysql restart"
		service mysql restart
	fi

else
	echo -e "\nMySQL is not installed. No databases imported."
fi

# Run wp-cli as vagrant user
if (( $EUID == 0 )); then
    wp() { sudo -EH -u vagrant -- wp "$@"; }
fi

if [[ $ping_result == "Connected" ]]; then
	# WP-CLI Install
	if [[ ! -d /srv/wp-cli ]]; then
		echo -e "\nDownloading wp-cli, see http://wp-cli.org"
		git clone git://github.com/wp-cli/wp-cli.git /srv/wp-cli
		cd /srv/wp-cli
		composer install
	else
		echo -e "\nUpdating wp-cli..."
		cd /srv/wp-cli
		git pull --rebase origin master
		composer update
	fi
	# Link `wp` to the `/usr/local/bin` directory
	ln -sf /srv/wp-cli/bin/wp /usr/local/bin/wp

else
	echo -e "\nNo network available, skipping network installations"
fi

end_seconds="$(date +%s)"
echo "-----------------------------"
echo "Provisioning complete in "$(expr $end_seconds - $start_seconds)" seconds"
if [[ $ping_result == "Connected" ]]; then
	echo "External network connection established, packages up to date."
else
	echo "No external network available. Package installation and maintenance skipped."
fi